package com.mbds.tpt_sarino_brian_hery.viewmodel.citoyen;

import android.content.Context;
import android.util.Log;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.mbds.tpt_sarino_brian_hery.model.citoyen.Citoyen;
import com.mbds.tpt_sarino_brian_hery.model.citoyen.CitoyenService;
import com.mbds.tpt_sarino_brian_hery.model.declareevent.DeclareEvent;
import com.mbds.tpt_sarino_brian_hery.model.services.TokenManager;
import com.mbds.tpt_sarino_brian_hery.model.utils.RetrofitInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;
import java.util.List;

public class CitoyenViewModel extends ViewModel {
    private MutableLiveData<Citoyen> citoyenLiveData;
    private CitoyenService citoyenService;

    public CitoyenViewModel() {
        citoyenService = RetrofitInstance.getRetrofitInstance().create(CitoyenService.class);
    }

    /**
     * Obtenir le citoyen.
     * @return
     */
    public LiveData<Citoyen> findCitoyen(Context context, int identifiant) {
        if (citoyenLiveData == null) {
            citoyenLiveData = new MutableLiveData<>();
        }
        getCitoyenByIdentifiant(context, identifiant); // Déplacez cette ligne en dehors de la condition if
        return citoyenLiveData;
    }

    /**
     * Obtenir un citoyen par son identifiant
     */
    public void getCitoyenByIdentifiant(Context context, int identifiant) {
        TokenManager tokenManager = new TokenManager(context);
        String token = tokenManager.getAccessToken();
        Call<Citoyen> call = citoyenService.getCitoyenByIdentifiant("Bearer " + token, identifiant);
        call.enqueue(new Callback<Citoyen>() {
            @Override
            public void onResponse(Call<Citoyen> call, Response<Citoyen> response) {
                if (response.isSuccessful()) {
                    Log.d("CitoyenViewModel onResponse success", "******************Loaded citoyen: " + response.body());
                    Citoyen citoyen = response.body();
                    citoyenLiveData.setValue(citoyen);

                } else {
                    citoyenLiveData.setValue(null);
                    // Gérer l'erreur ici, par exemple :
                    Log.e("CitoyenViewModel onResponse", "************Failed to load citoyen: " + response.message());
                    if (response.errorBody() != null) {
                        try {
                            String errorBody = response.errorBody().string();
                            Log.e("CitoyenViewModel onResponse", "Error Body: " + errorBody);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Citoyen> call, Throwable t) {
                Log.e("CitoyenViewModel onFailure", "************Failed to load citoyen: " + t.getMessage());
            }
        });
    }

}
