package com.mbds.tpt_sarino_brian_hery.viewmodel.user;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;
import androidx.lifecycle.ViewModel;
import com.mbds.tpt_sarino_brian_hery.model.services.TokenManager;
import com.mbds.tpt_sarino_brian_hery.model.services.UserResponse;
import com.mbds.tpt_sarino_brian_hery.model.user.UserService;
import com.mbds.tpt_sarino_brian_hery.model.user.User;
import com.mbds.tpt_sarino_brian_hery.model.utils.RetrofitInstance;
import com.mbds.tpt_sarino_brian_hery.view.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.core.content.ContextCompat.startActivity;

public class UserViewModel extends ViewModel {
//    private static AccesLocal accesLocal;
    private static User user;

    private static Context context;

    private static UserService userService;


    public UserViewModel(Context context) {
        this.context = context;
        userService = RetrofitInstance.getRetrofitInstance().create(UserService.class);
    }
    public void login(Context context,String username, String password) {
        user = new User();
        user.setUsername(username);
        user.setPassword(password);

        Call<UserResponse> call = userService.login(user);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if (response.isSuccessful()) {
                    UserResponse userResponse = response.body();
                    Log.d("UserViewModel", "****************** Successful login: " + userResponse.toString());
                    String accessToken = userResponse.getAccessToken();
                    if (accessToken != null) {
                        TokenManager tokenManager = new TokenManager(context);
                        tokenManager.saveAccessToken(accessToken);


                        Toast.makeText(context, "Vous êtes connecté " + username, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(context, MainActivity.class);
                        startActivity(context, intent, null);
                    }
                } else {
                    Log.e("UserViewModel", "*************** Error login onResponse: " + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                Log.e("UserViewModel", "*************** Error login onFailure: " + t.getMessage());
            }
        });
    }
/*
    *//**
     * Créer un nouveau compte.
     *//*
    public static void createAccount(User user) {
        accesLocal.ajouterUser(user);
    }*/

}
