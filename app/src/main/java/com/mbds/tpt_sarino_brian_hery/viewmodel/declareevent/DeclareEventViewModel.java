package com.mbds.tpt_sarino_brian_hery.viewmodel.declareevent;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.mbds.tpt_sarino_brian_hery.model.declareevent.DeclareEvent;
import com.mbds.tpt_sarino_brian_hery.model.declareevent.DeclareEventService;
import com.mbds.tpt_sarino_brian_hery.model.declareevent.DeclareEventServiceLocal;
import com.mbds.tpt_sarino_brian_hery.model.services.TokenManager;
import com.mbds.tpt_sarino_brian_hery.model.services.UserResponse;
import com.mbds.tpt_sarino_brian_hery.model.utils.RetrofitInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;

public class DeclareEventViewModel extends ViewModel {
    private MutableLiveData<List<DeclareEvent>> declareEventLiveData;
    private DeclareEventServiceLocal declareEventServiceLocal;
    private DeclareEventService declareEventService;

    private static final String DATABASE_NAME = "tpt_sarino_brian_hery";
    private static final int DATABASE_VERSION = 1;
    public DeclareEventViewModel(Context context) {
//        declareEventServiceLocal = new DeclareEventServiceLocal(context, DATABASE_NAME, null, DATABASE_VERSION);
        declareEventService = RetrofitInstance.getRetrofitInstance().create(DeclareEventService.class);
    }
    public DeclareEventViewModel() {
        declareEventService = RetrofitInstance.getRetrofitInstance().create(DeclareEventService.class);
    }


    /**
     * Rafréchir la liste des evenements.
     */
    /**
     * Charger la liste des merchants.
     */
    private void loadMerchants() {
        // Appel asynchrone pour récupérer les merchants via l'API
        Call<List<DeclareEvent>> call = declareEventService.findAll();
        call.enqueue(new Callback<List<DeclareEvent>>() {
            @Override
            public void onResponse(Call<List<DeclareEvent>> call, Response<List<DeclareEvent>> response) {
                if (response.isSuccessful()) {
                    List<DeclareEvent> events = response.body();
                    Log.d("MerchantViewModel", "******************Loaded events: " + events);
                    if (events == null) {
                        return;
                    } else {
                        declareEventLiveData.setValue(events);
                    }
//                    Toast.makeText(context, "Liste des merchants", Toast.LENGTH_SHORT).show();
                } else {
                    // Gérer l'erreur ici, par exemple :
                    Log.e("MerchantViewModel", "************Failed to load merchants: " + response.message());
//                    Toast.makeText(context, "Erreur lors de la récupération des merchants", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<DeclareEvent>> call, Throwable t) {
                // Gérer l'erreur ici, par exemple :
                Log.e("MerchantViewModel", "***************Error loading merchants: " + t.getMessage());
//                Toast.makeText(context, "Erreur du réseau", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Obtenir la liste des evenements.
     * @return
     */
    public LiveData<List<DeclareEvent>> findAll() {
        if (declareEventLiveData == null) {
            declareEventLiveData = new MutableLiveData<>();
            loadMerchants();
        } else {
            Log.e("MerchantViewModel", "******************Already loaded merchants");
        }
        return declareEventLiveData;
    }

    /**
     * Ajouter un evenement.
     */
    public DeclareEvent add(DeclareEvent event) {

        event = declareEventServiceLocal.addDeclareEvent(event);

        if (event != null) {
            loadMerchants();
            return event;
        } else {
            return null;
        }
    }

    /**
     *  Obtenir un evenement à partir de son id.
     */
    public DeclareEvent findById(int id) {
        return declareEventServiceLocal.findById(id);
    }

    /**
     * Declare évenement à envoyer vers l'API.
     */
    public void addEventAPI(Context context, DeclareEvent event) {
        TokenManager tokenManager = new TokenManager(context);
        String token = tokenManager.getAccessToken();

        Call<DeclareEvent> call = declareEventService.create("Bearer " + token, event);
        call.enqueue(new Callback<DeclareEvent>() {
            @Override
            public void onResponse(Call<DeclareEvent> call, Response<DeclareEvent> response) {
                if (response.isSuccessful()) {
                    DeclareEvent responseDeclareEvent = response.body();
                    Toast.makeText(context, "Evènement ajouté " + responseDeclareEvent.getDateEvent(), Toast.LENGTH_SHORT).show();
                    Log.d("UserViewModel", "****************** Declare event added: " + responseDeclareEvent.toString());
                    loadMerchants();
                } else {
                    Toast.makeText(context, "Evènement non ajouté", Toast.LENGTH_SHORT).show();
                    Log.e("UserViewModel", "*************** Error addEvent: " + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<DeclareEvent> call, Throwable t) {
                Log.e("UserViewModel", "*************** Error addEvent onFailure: " + t.getMessage());
            }
        });
    }
}
