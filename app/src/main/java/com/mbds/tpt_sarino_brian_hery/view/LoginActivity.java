package com.mbds.tpt_sarino_brian_hery.view;

import android.content.Intent;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.lifecycle.ViewModelProviders;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.mbds.tpt_sarino_brian_hery.R;
import com.mbds.tpt_sarino_brian_hery.model.services.TokenManager;
import com.mbds.tpt_sarino_brian_hery.viewmodel.user.UserViewModel;

import java.util.Arrays;
import java.util.List;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);
        TokenManager tokenManager = new TokenManager(this);
        String accessToken = tokenManager.getAccessToken();

        if (accessToken != null) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        init();
    }

    FirebaseAuth firebaseAuth;
    private UserViewModel userViewModel;
    TextView lnkRegister;
    EditText txtUsername, txtPwd;
    private static final int RC_SIGN_IN = 123; // Un code de demande arbitraire
    private ProgressBar progressBar;

    private void init() {
        firebaseAuth = FirebaseAuth.getInstance();

        userViewModel = new UserViewModel(this);
//        lnkRegister = findViewById(R.id.lnkRegister);
        txtUsername = findViewById(R.id.txtUsernameLogin);
        txtPwd = findViewById(R.id.txtPwdLogin);
        progressBar = findViewById(R.id.progress_bar_login);

        login();
//        goToRegister();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            if (resultCode == RESULT_OK) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                Toast.makeText(this, "Vous êtes connecté" + user.getEmail(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish(); // Terminer LoginActivity pour empêcher le retour
            } else {
                Toast.makeText(this, "Vous n'êtes pas connecté", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Se connecter avec un compte existant.
     */
    private void login() {
        findViewById(R.id.btnLogin).setOnClickListener(v -> {
            showProgressBar();
            String username = txtUsername.getText().toString();
            String pwd = txtPwd.getText().toString();
            if (checkFields(username, pwd)) {

                userViewModel.login(this,username, pwd);
                hideProgressBar();
            } else {
                Toast.makeText(this, "Veuillez remplir tous les champs", Toast.LENGTH_SHORT).show();
                hideProgressBar();
            }
        });
    }

    /**
     * Controle des champs.
     */
    public boolean checkFields(String email, String pwd) {
        boolean isValid = true;
        if (TextUtils.isEmpty(email)) {
            txtUsername.setError("Entrer un e-mail");
            txtUsername.requestFocus();
            isValid = false;
        }
        if (TextUtils.isEmpty(pwd)) {
            txtPwd.setError("Entrer un mot de passe");
            txtPwd.requestFocus();
            isValid = false;
        }

        return isValid;
    }


    /**
     * Redirection vers la page d'inscription.
     */
    public void goToRegister() {
        lnkRegister.setOnClickListener(v -> {
            startActivity(new Intent(this, RegisterActivity.class));
        });
    }



    private void showProgressBar(){
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar(){
        progressBar.setVisibility(View.GONE);
    }
}