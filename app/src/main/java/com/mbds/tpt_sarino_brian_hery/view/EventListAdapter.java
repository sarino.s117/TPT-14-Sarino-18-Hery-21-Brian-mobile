package com.mbds.tpt_sarino_brian_hery.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.mbds.tpt_sarino_brian_hery.R;
import com.mbds.tpt_sarino_brian_hery.model.declareevent.DeclareEvent;

import java.util.List;

public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.EventViewHolder> {
    private List<DeclareEvent> eventList;

    public EventListAdapter() {
    }
    public EventListAdapter(List<DeclareEvent> eventList) {
        this.eventList = eventList;
    }

    public void setEventList(List<DeclareEvent> eventList) {
        this.eventList = eventList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public EventViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_event, parent, false);
        return new EventViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull EventViewHolder holder, int position) {
        DeclareEvent event = eventList.get(position);
        holder.descriptionTextView.setText(event.getDescription());
        holder.lieuTextView.setText(event.getLieuEvent());
        holder.dateTextView.setText(event.getDateEvent());
    }

    @Override
    public int getItemCount() {
        return eventList != null ? eventList.size() : 0;
    }

    static class EventViewHolder extends RecyclerView.ViewHolder {
        TextView descriptionTextView;
        TextView lieuTextView;
        TextView dateTextView;

        EventViewHolder(@NonNull View itemView) {
            super(itemView);
            descriptionTextView = itemView.findViewById(R.id.descriptionTextView);
            lieuTextView = itemView.findViewById(R.id.lieuTextView);
            dateTextView = itemView.findViewById(R.id.dateTextView);
        }
    }
}
