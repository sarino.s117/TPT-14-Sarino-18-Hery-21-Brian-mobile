package com.mbds.tpt_sarino_brian_hery.view;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import com.mbds.tpt_sarino_brian_hery.R;
import com.mbds.tpt_sarino_brian_hery.model.citoyen.Citoyen;
import com.mbds.tpt_sarino_brian_hery.model.utils.RetrofitInstance;
import com.mbds.tpt_sarino_brian_hery.viewmodel.citoyen.CitoyenViewModel;

public class CitizenSearchFragment extends Fragment {

    private EditText searchField;
    private TextView resultText;
    private CitoyenViewModel citoyenViewModel;

    public CitizenSearchFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_search_citizen, container, false);

        citoyenViewModel = new ViewModelProvider(this).get(CitoyenViewModel.class);

        searchField = rootView.findViewById(R.id.search_field);
        resultText = rootView.findViewById(R.id.result_text);

//        citoyenViewModel = RetrofitInstance.getRetrofitInstance().create(CitoyenViewModel.class);

        // Gérer la recherche lorsqu'un bouton de recherche est cliqué
        rootView.findViewById(R.id.search_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setEnabled(false);
                String searchText = searchField.getText().toString().trim(); // Récupérez la valeur du champ de recherche et supprimez les espaces
                if (!searchText.isEmpty()) { // Vérifiez si la chaîne n'est pas vide
                    try {
                        int citizenId = Integer.parseInt(searchText); // Convertissez la chaîne en entier
                        if (citizenId < 0) {
                            resultText.setText("Aucun citoyen trouvé");
                            searchField.setText("");
                            searchField.requestFocus();
                        } else {
                            performSearch(citizenId); // Passez l'ID du citoyen à la fonction performSearch
                        }
                    } catch (NumberFormatException e) {
                        resultText.setText("ID de citoyen invalide");
                    }
                } else {
                    resultText.setText("Veuillez entrer un ID de citoyen");
                    searchField.requestFocus();
                }
                v.setEnabled(true);
            }
        });


        // Dans la méthode onCreateView de CitizenSearchFragment

        /*String searchText = searchField.getText().toString().trim(); // Récupérez la valeur du champ de recherche et supprimez les espaces
        if (!searchText.isEmpty()) { // Vérifiez si la chaîne n'est pas vide
            try {
                int citizenId = Integer.parseInt(searchText);
                citoyenViewModel.findCitoyen(requireContext() ,citizenId).observe(getViewLifecycleOwner(), new Observer<Citoyen>() {
                    @Override
                    public void onChanged(Citoyen citoyen) {
                // Mettez à jour votre interface utilisateur avec les données du citoyen
                if (citoyen != null) {
                    resultText.setText("Nom: " + citoyen.getNom() + "\n" +
                            "Prénom: " + citoyen.getPrenom() + "\n" +
                            "Adresse: " + citoyen.getAdresse() + "\n" +
                            "Date de naissance: " + citoyen.getDateNaissance());
                } else {
                    resultText.setText("Aucun citoyen trouvé");
                }
            }
                });
            } catch (NumberFormatException e) {
                resultText.setText("ID de citoyen invalide");
            }
        }*/

        return rootView;
    }

    private void performSearch(int idCitoyen) {
        citoyenViewModel.findCitoyen(requireContext(), idCitoyen).observe(getViewLifecycleOwner(), new Observer<Citoyen>() {
            @Override
            public void onChanged(Citoyen citoyen) {
                // Mettez à jour votre interface utilisateur avec les données du citoyen
                if (citoyen != null) {
                    resultText.setText("Nom: " + citoyen.getNom() + "\n" +
                            "Prénom: " + citoyen.getPrenom() + "\n" +
                            "Adresse: " + citoyen.getAdresse() + "\n" +
                            "Date de naissance: " + citoyen.getDateNaissance());
                } else {
                    resultText.setText("Aucun citoyen trouvé");
                }
            }
        });
    }
}