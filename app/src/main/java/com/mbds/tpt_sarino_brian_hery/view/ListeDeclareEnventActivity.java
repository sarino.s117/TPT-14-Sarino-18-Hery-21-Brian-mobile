package com.mbds.tpt_sarino_brian_hery.view;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.mbds.tpt_sarino_brian_hery.R;
import com.mbds.tpt_sarino_brian_hery.model.declareevent.DeclareEvent;
import com.mbds.tpt_sarino_brian_hery.viewmodel.declareevent.DeclareEventViewModel;

import java.util.List;

public class ListeDeclareEnventActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private EventListAdapter eventListAdapter;
    private DeclareEventViewModel eventListViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_declare_envent);
        eventListViewModel = ViewModelProviders.of(this).get(DeclareEventViewModel.class);;
        init();
    }

    private void init() {
        progressBar = findViewById(R.id.progress_bar);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(eventListAdapter);

        // Observer pour surveiller les changements dans la liste des événements
        eventListViewModel.findAll().observe(this, new Observer<List<DeclareEvent>>() {
            @Override
            public void onChanged(List<DeclareEvent> eventList) {
                // Mettez en place un adaptateur pour afficher la liste des événements dans RecyclerView
                EventListAdapter adapter = new EventListAdapter(eventList);
                recyclerView.setAdapter(adapter);
                progressBar.setVisibility(View.GONE);
            }
        });

        findViewById(R.id.ic_back_listevent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
 /*   private void onEventListChanged(List<DeclareEvent> eventList) {
        eventListAdapter.setEventList(eventList);
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }*/
}