package com.mbds.tpt_sarino_brian_hery.view;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.mbds.tpt_sarino_brian_hery.R;
import com.mbds.tpt_sarino_brian_hery.model.declareevent.DeclareEvent;
import com.mbds.tpt_sarino_brian_hery.model.declareevent.DeclareEventService;
import com.mbds.tpt_sarino_brian_hery.model.declareevent.DeclareEventServiceLocal;
import com.mbds.tpt_sarino_brian_hery.model.utils.RetrofitInstance;
import com.mbds.tpt_sarino_brian_hery.viewmodel.declareevent.DeclareEventViewModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class EventDeclarationFragment extends Fragment {

    private EditText idCitizenField;
    private EditText descriptionField;
    private EditText lieuField;
    private String dateField;
    private DeclareEventServiceLocal declareEventServiceLocal;
    private DeclareEventViewModel declareEventViewModel;

    private ProgressBar progressBar;

    public EventDeclarationFragment() {
        // Constructeur vide requis
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_declare_event, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View rootView) {
        idCitizenField = rootView.findViewById(R.id.idcitizen_field);
        descriptionField = rootView.findViewById(R.id.description_field);
        lieuField = rootView.findViewById(R.id.lieu_field);

        progressBar = (ProgressBar) rootView.findViewById(R.id.progress_declare_event);

        // Obtenir la date actuelle
        Date date = new Date();
        // Objet SimpleDateFormat avec le format souhaité
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        // Date en utilisant le format spécifié
        dateField = dateFormat.format(date);

        declareEventServiceLocal = new DeclareEventServiceLocal(getContext(), null, null, 1);
        declareEventViewModel = new DeclareEventViewModel(getContext());

        ajouterEventButton(rootView);
        goToMyListe(rootView);
    }

    /**
     * Ajout d'un évènement
     */
    private void ajouterEventButton(View rootView) {
        rootView.findViewById(R.id.submit_event_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setEnabled(false);
                showProgressBar();
                String idCitizenText = idCitizenField.getText().toString().trim();
                String description = descriptionField.getText().toString();
                String lieu = lieuField.getText().toString();
                if (checkFields(idCitizenText, description, lieu, dateField)) {
                    int idCitizen = Integer.parseInt(idCitizenText);
                    DeclareEvent event = new DeclareEvent(idCitizen, description, lieu, dateField);
//                    event = declareEventServiceLocal.addDeclareEvent(event);
                    declareEventViewModel.addEventAPI(requireContext(),event);
                   /* if (event != null) {
                        idCitizenField.setText("");
                        descriptionField.setText("");
                        lieuField.setText("");
                        Toast.makeText(getContext(), "Evenement ajouté", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), "Evenement non ajouté", Toast.LENGTH_SHORT).show();
                    }*/

                    idCitizenField.setText("");
                    descriptionField.setText("");
                    lieuField.setText("");
                    hideProgressBar();
                    v.setEnabled(true);
                } else {
                    v.setEnabled(true);
                    hideProgressBar();
                }

            }
        });
    }

    /**
     * Est-ce que les champs réquis sont valides.
     */
    private boolean checkFields(String idCitizen, String description, String lieuEvent, String dateEvent) {
        boolean isValid = true;
        if (TextUtils.isEmpty(idCitizen)) {
            idCitizenField.setError("Entrer un identifiant de citoyen");
            idCitizenField.requestFocus();
            isValid = false;
        }
        if (TextUtils.isEmpty(description)) {
            descriptionField.setError("Entrer une description");
            descriptionField.requestFocus();
            isValid = false;
        }
        if (TextUtils.isEmpty(lieuEvent)) {
            lieuField.setError("Entrer un lieu");
            lieuField.requestFocus();
            isValid = false;
        }

        return isValid;
    }

    /**
     * Aller vers la page de la liste des évènements déclarés par l'utilisateur connecté.
     */
    private void goToMyListe(View viewRoot) {
        viewRoot.findViewById(R.id.myliste_button).setOnClickListener(view -> {
            Intent intent = new Intent(getContext(), ListeDeclareEnventActivity.class);
            startActivity(intent);
        });
    }


    private void showProgressBar(){
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar(){
        progressBar.setVisibility(View.GONE);
    }
}