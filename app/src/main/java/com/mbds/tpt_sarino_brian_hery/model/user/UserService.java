package com.mbds.tpt_sarino_brian_hery.model.user;

import com.mbds.tpt_sarino_brian_hery.model.services.UserResponse;
import com.mbds.tpt_sarino_brian_hery.model.user.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserService {

    @POST("auth/signin")
    Call<UserResponse> login(@Body User user);

}
