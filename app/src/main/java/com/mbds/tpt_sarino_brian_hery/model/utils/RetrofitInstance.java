    package com.mbds.tpt_sarino_brian_hery.model.utils;

    import retrofit2.Retrofit;
    import retrofit2.converter.gson.GsonConverterFactory;

    public class RetrofitInstance {

        static String host1 = "http://10.0.0.160:8080"; // local
        static String host2 = "https://e-gouvernance-api-14-sarino-18-hery-21.onrender.com"; // réseau sur le télephone
        private static final String BASE_URL = host2 + "/api/";
        private static Retrofit retrofit;

        /**
         * Obtenir un instance de la classe Retrofit
         * @return
         */
        public static Retrofit getRetrofitInstance(){
            if(retrofit == null){
                retrofit = new Retrofit.Builder()
                        .addConverterFactory(GsonConverterFactory.create())
                        .baseUrl(BASE_URL)
                        .build();
            }
            return retrofit;
        }
    }
