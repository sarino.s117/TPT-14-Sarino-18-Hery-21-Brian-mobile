package com.mbds.tpt_sarino_brian_hery.model.declareevent;

import javax.validation.constraints.NotNull;

public class DeclareEvent {
    private int id;
    private int idCitoyen;
    private String description;
    private String lieuEvent;
    @NotNull(message = "La date de l'événement ne peut pas être nulle.")
    private String dateEvent;

    public DeclareEvent() {
    }

    public DeclareEvent(int idCitoyen, String description, String lieuEvent, String dateEvent) {
        this.idCitoyen = idCitoyen;
        this.description = description;
        this.lieuEvent = lieuEvent;
        this.dateEvent = dateEvent;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCitoyen() {
        return idCitoyen;
    }

    public void setIdCitoyen(int idCitoyen) {
        this.idCitoyen = idCitoyen;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLieuEvent() {
        return lieuEvent;
    }

    public void setLieuEvent(String lieuEvent) {
        this.lieuEvent = lieuEvent;
    }

    public String getDateEvent() {
        return dateEvent;
    }

    public void setDateEvent(String dateEvent) {
        this.dateEvent = dateEvent;
    }

    @Override
    public String toString() {
        return dateEvent + " " + lieuEvent;
    }
}
