package com.mbds.tpt_sarino_brian_hery.model.citoyen;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface CitoyenService {

    /**
     * Obtenir un citoyen par son identifiant
     */
    @GET("citoyen/{id}")
    Call<Citoyen> getCitoyenByIdentifiant(@Header("Authorization") String token, @Path("id") int id);

}
