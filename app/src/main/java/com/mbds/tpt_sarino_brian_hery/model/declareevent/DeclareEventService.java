package com.mbds.tpt_sarino_brian_hery.model.declareevent;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

import java.util.List;

public interface DeclareEventService {

    @POST("event/create")
    Call<DeclareEvent> create(@Header("Authorization") String token, @Body DeclareEvent declareEvent);

    @GET("event/all")
    Call<List<DeclareEvent>> findAll();
}
