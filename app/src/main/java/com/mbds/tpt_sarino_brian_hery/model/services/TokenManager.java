package com.mbds.tpt_sarino_brian_hery.model.services;

import android.content.Context;
import android.content.SharedPreferences;

public class TokenManager {
    private static final String TOKEN_PREFERENCES = "token_preferences";
    private static final String ACCESS_TOKEN = "access_token";

    private final SharedPreferences sharedPreferences;

    public TokenManager(Context context) {
        sharedPreferences = context.getSharedPreferences(TOKEN_PREFERENCES, Context.MODE_PRIVATE);
    }

    public void saveAccessToken(String accessToken) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ACCESS_TOKEN, accessToken);
        editor.apply();
    }

    public String getAccessToken() {
        return sharedPreferences.getString(ACCESS_TOKEN, null);
    }

    public void clearToken() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(ACCESS_TOKEN);
        editor.apply();
    }
}
